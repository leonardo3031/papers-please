﻿using System;
using DocumentValidator.Utils.Exceptions;

namespace DocumentValidator
{
    public class Document
    {
        public string EntrantName { get; set; }
        public DateTime ExpirationDate { get; set; }
        
        public void ValidateExpirationDate(string documentName)
        {
            var validationDate = new DateTime(1982, 11, 22);

            if(ExpirationDate <= validationDate)
                throw new InvalidDocumentException($"{documentName} expired", false);
        }
    }
}

﻿using System;

namespace DocumentValidator.Documents
{
    public class Passport : Document
    {
        public string Id { get; set; }
        public string Nation { get; set; }
        public DateTime DateOfBirth { get; set; }
        public char Sex { get; set; }
        public string Iss { get; set; }
    }
}
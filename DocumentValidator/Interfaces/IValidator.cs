﻿using BulletinManager;

namespace DocumentValidator.Interfaces
{
    public interface IValidator
    {
        void ValidatePassport(Rules rules, Entrant entrant);
        void ValidateCertificateOfVaccination(Rules rules, Entrant entrant);
        void ValidateIdCard(Entrant entrant);
        void ValidateAccessPermit(Entrant entrant);
        void ValidateWorkPass(Entrant entrant);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using BulletinManager;
using BulletinManager.Utils;
using DocumentValidator;
using DocumentValidator.Documents;
using DocumentValidator.Interfaces;
using DocumentValidator.Utils;
using DocumentValidator.Utils.Exceptions;
using NUnit.Framework;

namespace Tests
{
    public class DocumentValidatorTests
    {
        private readonly IValidator _validator = new Validator();

        [SetUp]
        public void Setup()
        {
            Rules.GetRules.AllowEntryTo = new List<string>();
            Rules.GetRules.DenyEntryTo = new List<string>();
            Rules.GetRules.RequiredDocuments = new List<RequiredDocument>();
            Rules.GetRules.RequiredVaccinations = new List<Vaccination>();
            Rules.GetRules.WantedCriminals = new List<string>();
        }

        [Test]
        public void AllDocuments_MissingDocument_ThrowMissingDocumentException()
        {
            var entrant = new Entrant();

            Assert.Throws<MissingDocumentException>(() => _validator.ValidatePassport(Rules.GetRules, entrant));
            Assert.Throws<MissingDocumentException>(() => _validator.ValidateCertificateOfVaccination(Rules.GetRules, entrant));
            Assert.Throws<MissingDocumentException>(() => _validator.ValidateIdCard(entrant));
            Assert.Throws<MissingDocumentException>(() => _validator.ValidateAccessPermit(entrant));
            Assert.Throws<MissingDocumentException>(() => _validator.ValidateWorkPass(entrant));
        }

        [Test]
        public void AllDocuments_ExpiredDocument_ThrowInvalidDocumentException()
        {
            var entrant = new Entrant
            {
                Passport = new Passport
                {
                    ExpirationDate = new DateTime(1981, 5, 1)
                },
                CertificateOfVaccination = new CertificateOfVaccination
                {
                    ExpirationDate = new DateTime(1981, 5, 1)
                },
                IdCard = new IdCard
                {
                    ExpirationDate = new DateTime(1981, 5, 1)
                },
                AccessPermit = new AccessPermit
                {
                    ExpirationDate = new DateTime(1981, 5, 1)
                },
                WorkPass = new WorkPass
                {
                    ExpirationDate = new DateTime(1981, 5, 1)
                }
            };

            Assert.Throws<InvalidDocumentException>(() => _validator.ValidatePassport(Rules.GetRules, entrant));
            Assert.Throws<InvalidDocumentException>(() => _validator.ValidateCertificateOfVaccination(Rules.GetRules, entrant));
            Assert.Throws<InvalidDocumentException>(() => _validator.ValidateIdCard(entrant));
            Assert.Throws<InvalidDocumentException>(() => _validator.ValidateAccessPermit(entrant));
            Assert.Throws<InvalidDocumentException>(() => _validator.ValidateWorkPass(entrant));
        }

        [Test]
        public void ValidatePassport_EntrantIsWantedCriminal_ThrowInvalidDocumentException()
        {
            var rules = Rules.GetRules;
            rules.AllowEntryTo.Add("Arstotzka");
            rules.WantedCriminals.Add("Vladimir Zadornov");

            var entrant = new Entrant
            {
                Passport = new Passport
                {
                    EntrantName = "Vladimir Zadornov",
                    ExpirationDate = new DateTime(1983, 1, 1),
                    Nation = "Arstotzka"
                }
            };

            Assert.Throws<InvalidDocumentException>(() => _validator.ValidatePassport(rules, entrant));
        }

        [Test]
        [TestCase("Kolechia")]
        [TestCase("Impor")]
        public void ValidatePassport_EntryNotAllowed_ThrowInvalidDocumentException(string entrantNation)
        {
            var rules = Rules.GetRules;
            rules.DenyEntryTo.Add("Kolechia");

            var entrant = new Entrant
            {
                Passport = new Passport {Nation = entrantNation, ExpirationDate = new DateTime(1983, 1, 1)}
            };

            Assert.Throws<InvalidDocumentException>(() => _validator.ValidatePassport(rules, entrant));
        }

        [Test]
        public void ValidateCertificateOfVaccination_MismatchingInformation_ThrowInvalidDocumentException()
        {
            var entrant1 = new Entrant
            {
                Passport = new Passport{ EntrantName = "Andron Scheutz" },
                CertificateOfVaccination = new CertificateOfVaccination
                {
                    EntrantName = "Andy Scheutz",
                    ExpirationDate = new DateTime(1983, 1, 1)
                }
            };

            var entrant2 = new Entrant
            {
                Passport = new Passport{ Id = "A425-548D-B457-R529" },
                CertificateOfVaccination = new CertificateOfVaccination
                {
                    PassportNumber = "A123-B456-C789-D012",
                    ExpirationDate = new DateTime(1983, 1, 1)
                }
            };

            Assert.Throws<InvalidDocumentException>(() => _validator.ValidateCertificateOfVaccination(Rules.GetRules, entrant1));
            Assert.Throws<InvalidDocumentException>(() => _validator.ValidateCertificateOfVaccination(Rules.GetRules, entrant2));
        }

        [Test]
        public void ValidateCertificateOfVaccination_EntrantDoesNotHaveRequiredVaccination_ThrowInvalidDocumentException()
        {
            var rules = Rules.GetRules;

            rules.RequiredVaccinations.Add(new Vaccination
            {
                Name = "polio",
                RequiredFor = new List<string>() { "Obristan" }
            });

            var entrantVaccines = new List<Vaccine>() {new Vaccine {VaccineName = "malaria"}};

            var entrant = new Entrant
            {
                Passport = new Passport{ Nation = "Obristan" },
                CertificateOfVaccination = new CertificateOfVaccination { 
                    Vaccines = entrantVaccines,
                    ExpirationDate = new DateTime(1983, 1, 1)
                }
            };

            Assert.Throws<InvalidDocumentException>(() => _validator.ValidateCertificateOfVaccination(rules, entrant));
        }

        [Test]
        public void ValidateIdCard_MismatchingInformation_ThrowInvalidDocumentException()
        {
            var entrant1 = new Entrant
            {
                Passport = new Passport { EntrantName = "Andron Scheutz" },
                IdCard = new IdCard { EntrantName = "Andy Scheutz", ExpirationDate = new DateTime(1983, 1, 1) }
            };

            var entrant2 = new Entrant
            {
                Passport = new Passport { DateOfBirth = new DateTime(1844, 2, 27)},
                IdCard = new IdCard { DateOfBirth = new DateTime(1837, 10, 12), ExpirationDate = new DateTime(1983, 1, 1) }
            };

            Assert.Throws<InvalidDocumentException>(() => _validator.ValidateIdCard(entrant1));
            Assert.Throws<InvalidDocumentException>(() => _validator.ValidateIdCard(entrant2));
        }

        [Test]
        public void ValidateAccessPermit_MismatchingInformation_ThrowInvalidDocumentException()
        {
            var entrant1 = new Entrant
            {
                Passport = new Passport{ Id = "A425-548D-B457-R529" },
                AccessPermit = new AccessPermit
                {
                    PassportId = "A123-B456-C789-D012",
                    ExpirationDate = new DateTime(1983, 1, 1)
                }
            };

            var entrant2 = new Entrant
            {
                Passport = new Passport{ EntrantName = "Andron Scheutz" },
                AccessPermit = new AccessPermit
                {
                    EntrantName = "Andy Scheutz",
                    ExpirationDate = new DateTime(1983, 1, 1)
                }
            };

            Assert.Throws<InvalidDocumentException>(() => _validator.ValidateAccessPermit(entrant1));
            Assert.Throws<InvalidDocumentException>(() => _validator.ValidateAccessPermit(entrant2));
        }
    }
}
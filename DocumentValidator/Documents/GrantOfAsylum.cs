﻿using System;

namespace DocumentValidator.Documents
{
    public class GrantOfAsylum : Document
    {
        public string Nationality { get; set; }
        public string PassportNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}

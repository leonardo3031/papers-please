using System.Collections.Generic;
using BulletinManager;
using BulletinManager.Interfaces;
using BulletinManager.Utils;
using NUnit.Framework;

namespace Tests
{
    public class BulletinManagerTests
    {
        private readonly IManager _manager = new Manager();

        [SetUp]
        public void Setup()
        {
            Rules.GetRules.AllowEntryTo = new List<string>();
            Rules.GetRules.DenyEntryTo = new List<string>();
            Rules.GetRules.RequiredDocuments = new List<RequiredDocument>();
            Rules.GetRules.RequiredVaccinations = new List<Vaccination>();
            Rules.GetRules.WantedCriminals = new List<string>();
        }

        [Test]
        [TestCase("Allow citizens of United Federation", 1)]
        [TestCase("Allow citizens of Arstotzka, Obristan", 2)]
        public void ManageAllowedCountries_AllowCountries_AddCountriesToAllowedCountriesList(string bulletin, int expectedListLength)
        {
            var allowedEntryTo = Rules.GetRules.AllowEntryTo;

            _manager.ManageAllowedCountries(bulletin);

            Assert.That(allowedEntryTo, Is.Not.Empty);
            Assert.That(allowedEntryTo.Count, Is.EqualTo(expectedListLength));
        }

        [Test]
        [TestCase("Deny citizens of Arstotzka", 1)]
        [TestCase("Deny citizens of United Federation, Obristan", 2)]
        public void ManageAllowedCountries_DenyCountries_AddCountriesToDeniedCountriesList(string bulletin, int expectedListLength)
        {
            var deniedEntryTo = Rules.GetRules.DenyEntryTo;

            _manager.ManageAllowedCountries(bulletin);

            Assert.That(deniedEntryTo, Is.Not.Empty);
            Assert.That(deniedEntryTo.Count, Is.EqualTo(expectedListLength));
        }

        [Test]
        [TestCase("Citizens of Impor require polio vaccination", "polio", 1)]
        [TestCase("Citizens of Antegria, Republia, Kolechia require tetanus vaccination", "tetanus", 3)]
        public void ManageRequiredVaccinations_RequireVaccination_AddVaccinationToRequiredVaccinationsList(string bulletin, string expectedVaccinationName, int expectedRequiredForListLength)
        {
            var requiredVaccinations = Rules.GetRules.RequiredVaccinations;

            _manager.ManageRequiredVaccinations(bulletin);

            Assert.That(requiredVaccinations, Is.Not.Empty);
            Assert.That(requiredVaccinations.Count, Is.EqualTo(1));
            Assert.That(requiredVaccinations[0].Name, Is.EqualTo(expectedVaccinationName));
            Assert.That(requiredVaccinations[0].RequiredFor.Count, Is.EqualTo(expectedRequiredForListLength));
        }

        [Test]
        public void ManageRequiredVaccinations_EntrantsNoLongerRequireVaccination_RemoveVaccinationFromRequiredVaccinationsList()
        {
            var requiredVaccinations = Rules.GetRules.RequiredVaccinations;

            var vaccinationToBeRemoved = new Vaccination
            {
                Name = "polio",
                RequiredFor = new List<string> {"Antegria", "Republia", "Obristan"}
            };

            var vaccinationToBeKept = new Vaccination
            {
                Name = "tetanus",
                RequiredFor = new List<string> {"Kolechia", "Republia"}
            };

            requiredVaccinations.AddRange(new List<Vaccination> { vaccinationToBeRemoved, vaccinationToBeKept });

            _manager.ManageRequiredVaccinations("Entrants no longer require polio vaccination");

            Assert.That(requiredVaccinations.Count, Is.EqualTo(1));
            Assert.That(requiredVaccinations, Does.Not.Contain(vaccinationToBeRemoved));
            Assert.That(requiredVaccinations, Does.Contain(vaccinationToBeKept));
        }

        [Test]
        [TestCase("Wanted by the State: Hubert Popovic", 1)]
        [TestCase("Wanted by the State: Hubert Popovic, Vladimir Zadornov", 2)]
        public void ManageWantedCriminals_AddWantedCriminal_AddCriminalToWantedCriminalsList(string bulletin, int expectedListLegth)
        {
            var wantedCriminals = Rules.GetRules.WantedCriminals;

            _manager.ManageWantedCriminals(bulletin);

            Assert.That(wantedCriminals, Is.Not.Empty);
            Assert.That(wantedCriminals.Count, Is.EqualTo(expectedListLegth));
        }

        [Test]
        [TestCase("Entrants require passport", "Entrants", "passport")]
        [TestCase("Citizens of Arstotzka require ID card", "Citizens", "ID card")]
        [TestCase("Foreigners require access permit", "Foreigners", "access permit")]
        [TestCase("Workers require work pass", "Workers", "work pass")]
        public void ManageRequiredDocuments_AddRequiredDocument_AddDocumentToRequiredDocumentsList(string bulletin, string expectedRequiredForName, string expectedDocumentName)
        {
            var requiredDocuments = Rules.GetRules.RequiredDocuments;

            _manager.ManageRequiredDocuments(bulletin);

            Assert.That(requiredDocuments, Is.Not.Empty);
            Assert.That(requiredDocuments.Count, Is.EqualTo(1));
            Assert.That(requiredDocuments[0].RequiredFor, Is.EqualTo(expectedRequiredForName));
            Assert.That(requiredDocuments[0].RequiredDocuments, Does.Contain(expectedDocumentName));
        }

    }
}
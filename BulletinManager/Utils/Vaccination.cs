﻿using System.Collections.Generic;

namespace BulletinManager.Utils
{
    public class Vaccination
    {
        public string Name { get; set; }
        public List<string> RequiredFor { get; set; }
    }
}

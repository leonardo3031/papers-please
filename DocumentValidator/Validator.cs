﻿using BulletinManager;
using DocumentValidator.Interfaces;
using DocumentValidator.Utils;
using DocumentValidator.Utils.Exceptions;

namespace DocumentValidator
{
    public class Validator : IValidator
    {
        public virtual void ValidatePassport(Rules rules, Entrant entrant)
        {
            if (entrant.Passport == null)
                throw new MissingDocumentException(DocumentName.Passport);

            entrant.Passport.ValidateExpirationDate(DocumentName.Passport);

            var nation = entrant.Passport.Nation;

            if (rules.WantedCriminals.Contains(entrant.Passport.EntrantName))
                throw new InvalidDocumentException("entrant is a wanted criminal", true);

            if (rules.DenyEntryTo.Contains(nation) || !rules.AllowEntryTo.Contains(nation))
                throw new InvalidDocumentException("citizens of " + nation + " not allowed");
        }

        public virtual void ValidateCertificateOfVaccination(Rules rules, Entrant entrant)
        {
            var certificate = entrant.CertificateOfVaccination;
            var passport = entrant.Passport;

            if (certificate == null)
                throw new MissingDocumentException(DocumentName.CertificateOfVaccination);

            certificate.ValidateExpirationDate(DocumentName.CertificateOfVaccination);

            if (certificate.PassportNumber != passport.Id || certificate.EntrantName != passport.EntrantName)
                throw new InvalidDocumentException("mismatching information");

            rules.RequiredVaccinations.ForEach(vaccine =>
            {
                if (vaccine.RequiredFor.Contains(passport.Nation) &&
                    !certificate.Vaccines.Exists(x => x.VaccineName == vaccine.Name))
                    throw new InvalidDocumentException("missing required vaccination");
            });
        }

        public virtual void ValidateIdCard(Entrant entrant)
        {
            var idCard = entrant.IdCard;
            var passport = entrant.Passport;

            if (idCard == null)
                throw new MissingDocumentException(DocumentName.IdCard);

            idCard.ValidateExpirationDate(DocumentName.IdCard);

            if (passport.EntrantName != idCard.EntrantName || passport.DateOfBirth != idCard.DateOfBirth)
                throw new InvalidDocumentException("mismatching information");
        }

        public virtual void ValidateAccessPermit(Entrant entrant)
        {
            var accessPermit = entrant.AccessPermit;
            var passport = entrant.Passport;

            if (accessPermit == null)
            {
                if (entrant.GrantOfAsylum != null)
                    ValidateGrantOfAsylum(entrant);
                else if (entrant.DiplomaticAuthorization != null)
                    ValidateDiplomaticAuthorization(entrant);
                else
                    throw new MissingDocumentException(DocumentName.AccessPermit);
            }
            else
            {
                accessPermit.ValidateExpirationDate(DocumentName.AccessPermit);

                if (accessPermit.PassportId != passport.Id || accessPermit.EntrantName != passport.EntrantName)
                    throw new InvalidDocumentException("mismatching information");
            }
        }

        private void ValidateGrantOfAsylum(Entrant entrant)
        {
            var grantOfAsylum = entrant.GrantOfAsylum;
            var passport = entrant.Passport;

            grantOfAsylum.ValidateExpirationDate(DocumentName.GrantOfAsylum);

            if(grantOfAsylum.EntrantName != passport.EntrantName || grantOfAsylum.PassportNumber != passport.Id || grantOfAsylum.DateOfBirth != passport.DateOfBirth)
                throw new InvalidDocumentException("mismatching information");
        }

        private void ValidateDiplomaticAuthorization(Entrant entrant)
        {
            var diplomaticAuthorization = entrant.DiplomaticAuthorization;
            var passport = entrant.Passport;

            diplomaticAuthorization.ValidateExpirationDate(DocumentName.DiplomaticAuthorization);

            if(diplomaticAuthorization.PassportNumber != passport.Id || diplomaticAuthorization.EntrantName != passport.EntrantName || diplomaticAuthorization.IssuingCountry != passport.Nation)
                throw new InvalidDocumentException("mismatching information");

            if(!diplomaticAuthorization.CountriesGrantedAccessTo.Contains("Arstotzka"))
                throw new InvalidDocumentException("diplomatic authorization does not grant access to Arstotzka");
        }

        public virtual void ValidateWorkPass(Entrant entrant)
        {
            var workPass = entrant.WorkPass;

            if (workPass == null)
                throw new MissingDocumentException(DocumentName.WorkPass);

            workPass.ValidateExpirationDate(DocumentName.WorkPass);
        }
    }
}
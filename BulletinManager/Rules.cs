﻿using System.Collections.Generic;
using BulletinManager.Utils;

namespace BulletinManager
{
    public class Rules
    {
        private Rules()
        {
            AllowEntryTo = new List<string>();
            DenyEntryTo = new List<string>();
            RequiredDocuments = new List<RequiredDocument>();
            RequiredVaccinations = new List<Vaccination>();
            WantedCriminals = new List<string>();
        }

        public static Rules GetRules { get; } = new Rules();
        public List<string> AllowEntryTo { get; set; }
        public List<string> DenyEntryTo { get; set; }
        public List<RequiredDocument> RequiredDocuments { get; set; }
        public List<Vaccination> RequiredVaccinations { get; set; }
        public List<string> WantedCriminals { get; set; }
    }
}

﻿namespace DocumentValidator.Utils.Exceptions
{
    public class InvalidDocumentException : BaseDocumentException
    {
        public InvalidDocumentException(string reason)
        {
            Reason = $"Entry denied: {reason}";
        }

        public InvalidDocumentException(string reason, bool isDetained)
        {
            Reason = $"Detainment: {reason}";
        }
    }
}

﻿namespace DocumentValidator.Utils.Exceptions
{
    public class MissingDocumentException : BaseDocumentException
    {
        public MissingDocumentException(string documentName)
        {
            Reason = $"Entry denied: missing required {documentName}";
        }
    }
}

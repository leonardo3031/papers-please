﻿namespace BulletinManager.Interfaces
{
    public interface IManager
    {
        void ManageAllowedCountries(string rule);
        void ManageRequiredVaccinations(string rule);
        void ManageWantedCriminals(string rule);
        void ManageRequiredDocuments(string rule);
    }
}

﻿using BulletinManager;
using BulletinManager.Interfaces;
using DocumentValidator;
using DocumentValidator.Interfaces;
using DocumentValidator.Utils;
using DocumentValidator.Utils.Exceptions;
using Papers__please.Interfaces;

namespace Papers__please
{
    public class Inspector : IInspector
    {
        private readonly IManager _manager;
        private readonly IValidator _validator;

        public Inspector(IManager manager, IValidator validator)
        {
            _manager = manager;
            _validator = validator;
        }

        public virtual void ReceiveBulletin(string bulletin)
        {
            var rules = bulletin.Split("\n");

            foreach (var rule in rules)
            {
                if (rule.Contains("Allow citizens") || rule.Contains("Deny citizens"))
                {
                    _manager.ManageAllowedCountries(rule);
                }
                else if (rule.Contains("vaccination"))
                {
                    _manager.ManageRequiredVaccinations(rule);
                }
                else if (rule.Contains("Wanted by the State"))
                {
                    _manager.ManageWantedCriminals(rule);
                }
                else
                {
                    _manager.ManageRequiredDocuments(rule);
                }
            }
        }

        public virtual string Inspect(Entrant entrant)
        {
            var rules = Rules.GetRules;
            var nation = entrant.Passport?.Nation;

            try
            {
                rules.RequiredDocuments.ForEach(doc =>
                {
                    switch (doc.RequiredFor.ToLower())
                    {
                        case "entrants":
                            doc.RequiredDocuments.ForEach(d =>
                            {
                                if (d == DocumentName.Passport)
                                    _validator.ValidatePassport(rules, entrant);

                                if (d == DocumentName.CertificateOfVaccination)
                                    _validator.ValidateCertificateOfVaccination(rules, entrant);
                            });
                            break;
                        case "citizens":
                            if (nation == "Arstotzka")
                                doc.RequiredDocuments.ForEach(d =>
                                {
                                    if (d == DocumentName.IdCard)
                                        _validator.ValidateIdCard(entrant);
                                });
                            break;
                        case "foreigners":
                            if (nation != "Arstotzka")
                                doc.RequiredDocuments.ForEach(d =>
                                {
                                    if (d == DocumentName.AccessPermit)
                                        _validator.ValidateAccessPermit(entrant);
                                });
                            break;
                        case "workers":
                            if (entrant.AccessPermit != null && entrant.AccessPermit.Purpose.ToLower() == "work")
                                doc.RequiredDocuments.ForEach(d =>
                                {
                                    if (d == DocumentName.WorkPass)
                                        _validator.ValidateWorkPass(entrant);
                                });
                            break;
                    }
                });

                return PassInspection(nation ?? "");
            }
            catch (BaseDocumentException e)
            {
                return e.Reason;
            }
        }

        protected virtual string PassInspection(string entrantNation)
        {
            return entrantNation == "Arstotzka" ? "Glory to Arstotzka" : "Cause no trouble";
        }
    }
}
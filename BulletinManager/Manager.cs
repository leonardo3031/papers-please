﻿using System.Collections.Generic;
using System.Linq;
using BulletinManager.Interfaces;
using BulletinManager.Utils;

namespace BulletinManager
{
    public class Manager : IManager
    {
        public virtual void ManageAllowedCountries(string rule)
        {
            var mainStrings = rule.Split("of");
            var countries = mainStrings[1].Split(",").Select(x => x.Trim()).ToList();

            var rules = Rules.GetRules;

            if (mainStrings[0].Contains("Allow"))
            {
                foreach (var country in countries)
                {
                    if (rules.DenyEntryTo.Contains(country))
                        rules.DenyEntryTo.Remove(country);

                    rules.AllowEntryTo.Add(country);
                }
            }
            else
            {
                foreach (var country in countries)
                {
                    if (rules.AllowEntryTo.Contains(country))
                        rules.AllowEntryTo.Remove(country);
                    
                    rules.DenyEntryTo.Add(country);
                }
            }
        }

        public virtual void ManageRequiredVaccinations(string rule)
        {
            var requiredVaccinations = Rules.GetRules.RequiredVaccinations;

            if (!rule.Contains("no longer"))
            {
                var mainStrings = rule.Split("of")[1].Split("require");
                var countries = mainStrings[0].Split(",").Select(x => x.Trim()).ToList();
                var vaccinationName = mainStrings[1].Trim().Split(" ")[0].Trim();
            
                var vaccination = new Vaccination
                {
                    Name = vaccinationName,
                    RequiredFor = countries
                };

                requiredVaccinations.Add(vaccination);
            }
            else
            {
                var vaccinationName = rule.Split("require")[1].Trim().Split(" ")[0].Trim();

                requiredVaccinations.Remove(requiredVaccinations.First(x => x.Name.Contains(vaccinationName)));
            }
        }

        public virtual void ManageWantedCriminals(string rule)
        {
            var criminals = rule.Split(":")[1].Split(",").Select(x => x.Trim());
            
            Rules.GetRules.WantedCriminals.AddRange(criminals);
        }

        public virtual void ManageRequiredDocuments(string rule)
        {
            var mainStrings = rule.Split("require");
            var requiredFor = mainStrings[0].Split(" ")[0].Trim();
            var requiredDocument = mainStrings[1].Trim();

            var rulesReqDocs = Rules.GetRules.RequiredDocuments;

            var existingDoc = rulesReqDocs.FirstOrDefault(x => x.RequiredFor == requiredFor);

            if (existingDoc == null)
            {
                rulesReqDocs.Add(new RequiredDocument
                {
                    RequiredFor = requiredFor,
                    RequiredDocuments = new List<string>{requiredDocument}
                });
            }
            else
            {
                existingDoc.RequiredDocuments.Add(requiredDocument);
            }
        }
    }
}

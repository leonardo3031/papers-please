﻿using System.Collections.Generic;

namespace DocumentValidator.Documents
{
    public class DiplomaticAuthorization : Document
    {
        public string PassportNumber { get; set; }
        public string IssuingCountry { get; set; }
        public List<string> CountriesGrantedAccessTo { get; set; }
    }
}

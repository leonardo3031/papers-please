﻿using DocumentValidator.Documents;

namespace DocumentValidator
{
    public class Entrant
    {
        public Passport Passport { get; set; }
        public IdCard IdCard { get; set; }
        public AccessPermit AccessPermit { get; set; }
        public WorkPass WorkPass { get; set; }
        public GrantOfAsylum GrantOfAsylum { get; set; }
        public CertificateOfVaccination CertificateOfVaccination { get; set; }
        public DiplomaticAuthorization DiplomaticAuthorization { get; set; }
    }
}
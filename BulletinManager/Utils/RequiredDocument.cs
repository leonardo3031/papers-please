﻿using System.Collections.Generic;

namespace BulletinManager.Utils
{
    public class RequiredDocument
    {
        public string RequiredFor { get; set; }
        public List<string> RequiredDocuments { get; set; }
    }
}

﻿using System.Collections.Generic;
using DocumentValidator.Utils;

namespace DocumentValidator.Documents
{
    public class CertificateOfVaccination : Document
    {
        public string PassportNumber { get; set; }
        public List<Vaccine> Vaccines { get; set; }
    }
}

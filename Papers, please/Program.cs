﻿using System;
using System.Collections.Generic;
using BulletinManager;
using DocumentValidator;
using DocumentValidator.Documents;
using Papers__please.Interfaces;

namespace Papers__please
{
    static class Program
    {
        static void Main(string[] args)
        {
            IInspector inspector = new Inspector(new Manager(), new Validator());
            
            inspector.ReceiveBulletin("Entrants require passport\nForeigners require access permit\nWorkers require work pass\nCitizens of Arstotzka require ID card\nAllow citizens of Arstotzka, Kolechia");

            var citizen = new Entrant
            {
                Passport = new Passport
                {
                    Id = "GC07D-FU8AR",
                    Nation = "Arstotzka",
                    EntrantName = "Guyovich",
                    DateOfBirth = new DateTime(1933, 11, 28),
                    Sex = 'M',
                    Iss = "East Grestin",
                    ExpirationDate = new DateTime(1983, 07, 10)
                },
                IdCard = new IdCard
                {
                    EntrantName = "Guyovich",
                    DateOfBirth = new DateTime(1933, 11, 28),
                    ExpirationDate = new DateTime(1983, 07, 10)
                }
            };

            Console.WriteLine($"Citizen:\n{inspector.Inspect(citizen)}\n");

            inspector.ReceiveBulletin("Wanted by the State: Vladimir Zadornov");

            var foreigner = new Entrant
            {
                Passport = new Passport
                {
                    Id = "SA48D-A6OK5",
                    Nation = "Kolechia",
                    EntrantName = "Scarlett Witch",
                    DateOfBirth = new DateTime(1933, 11, 28),
                    Sex = 'F',
                    Iss = "West Grestin",
                    ExpirationDate = new DateTime(1983, 07, 10)
                },
                DiplomaticAuthorization = new DiplomaticAuthorization
                {
                    IssuingCountry = "Kolechia",
                    PassportNumber = "SA48D-A6OK5",
                    CountriesGrantedAccessTo = new List<string> { "Arstotzka" },
                    EntrantName = "Scarlett Witch",
                    ExpirationDate = new DateTime(1983, 07, 10)
                }
            };

            Console.WriteLine($"Foreigner:\n{inspector.Inspect(foreigner)}\n");

            inspector.ReceiveBulletin("Allow citizens of United Federation, Impor");

            var worker = new Entrant
            {
                Passport = new Passport
                {
                    Id = "SA48D-A6OK5",
                    Nation = "United Federation",
                    EntrantName = "Vladimir Zadornov",
                    DateOfBirth = new DateTime(1933, 11, 28),
                    Sex = 'M',
                    Iss = "Shingleton",
                    ExpirationDate = new DateTime(1983, 07, 10)
                },
                WorkPass = new WorkPass
                {
                    EntrantName = "Vladimir Zadornov",
                    ExpirationDate = new DateTime(1983, 07, 10),
                    Field = "Plumber"
                }
            };

            Console.WriteLine($"Worker:\n{inspector.Inspect(worker)}");
        }
    }
}
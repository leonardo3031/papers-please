﻿namespace DocumentValidator.Documents
{
    public class AccessPermit : Document
    {
        public string PassportId { get; set; }
        public string Purpose { get; set; }
        public string DurationOfStay { get; set; }
    }
}

﻿namespace DocumentValidator.Utils
{
    public class DocumentName
    {
        public const string Passport = "passport";
        public const string IdCard = "ID card";
        public const string AccessPermit = "access permit";
        public const string CertificateOfVaccination = "certificate of vaccination";
        public const string DiplomaticAuthorization = "diplomatic authorization";
        public const string GrantOfAsylum = "grant of asylum";
        public const string WorkPass = "work pass";
    }
}

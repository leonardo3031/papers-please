﻿using DocumentValidator;

namespace Papers__please.Interfaces
{
    public interface IInspector
    {
        void ReceiveBulletin(string bulletin);
        string Inspect(Entrant entrant);
    }
}
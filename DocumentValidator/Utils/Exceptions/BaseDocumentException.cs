﻿using System;

namespace DocumentValidator.Utils.Exceptions
{
    public class BaseDocumentException : Exception
    {
        public string Reason { get; set; }
    }
}
